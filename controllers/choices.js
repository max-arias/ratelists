"use strict";

let db      = require('../models')
  , choices = {};

choices.getAll = function(req, res) {
    db.Choice.findAll({
        include: [{
            model: db.List,
            where: { 
                id: req.params.listId,
                ownerId: req.decoded.id
            }
        }]
    }).then(lists => {
        res.json({lists: lists })
    }).catch(function (err) {
        res.json(err)
    });
};

choices.getByList = function(req, res) {
    db.Choice.findOne({
        include: [{
            model: db.List,
            where: { 
                id: req.params.listId,
                ownerId: req.decoded.id
            }
        }]
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

choices.get = function(req, res) {
    db.Choice.findAll({
        where: {
            id: req.params.id
        },
        include: [{
            model: db.List,
            where: { 
                id: req.params.listId,
                ownerId: req.decoded.id
            }
        }]
    }).then(result => {
        res.json(result.shift())
    }).catch(function (err) {
        res.json(err)
    });
};

// -- POST --
choices.create = function(req, res) {
    //TODO: How can I do this without making two queries?
    db.List.findOne({
        where: {
            id: req.params.listId,
            ownerId: req.decoded.id
        }
    }).then(list => {
        db.Choice.create({
            value: req.body.value,
            choiceType: req.body.choiceType,
            UserId: list.ownerId,
            ListId: list.id 
        }/*, {
            include: [{
                model: db.List,
                where: { 
                    id: req.params.listId,
                    ownerId: req.decoded.id
                }
            }]
        }*/).then(newChoice => {
            res.json(newChoice)
        }).catch(function (err) {
            res.json(err)
        });
    }).catch(function (err) {
        res.json(err)
    });
    db.Choice.create({
        value: req.body.value,
        choiceType: req.body.choiceType,
        UserId: req.decoded.id,
        ListId: req.params.listId //How can I query for this without making two queries?
    }, {
        include: [{
            model: db.List,
            where: { 
                id: req.params.listId,
                ownerId: req.decoded.id
            }
        }]
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

// -- DELETE --
choices.delete = function(req, res) {
    db.Choice.destroy({
        where: {
            id: req.params.id,
            ownerId: req.decoded.id
        }
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

// -- UPDATE --
choices.update = function(req, res) {
    db.Choice.update({
        value: req.body.value,
        choiceType: req.body.choiceType,
    }, {
        where: {
            id: req.params.id,
            UserId: req.decoded.id
        }
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json({err: err})
    });
};

module.exports = choices;

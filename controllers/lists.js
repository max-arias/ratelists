"use strict";

var  db      = require('../models')
   , lists   = {};

// -- GET --
lists.getAll = function(req, res) {
    db.List.findAll({
        where: {
            ownerId: req.decoded.id
        }
    }).then(lists => {
        res.json({lists: lists })
    }).catch(function (err) {
        res.json(err)
    });
};

lists.get = function(req, res) {
    db.List.findAll({
        where: {
            id: req.params.id,
            ownerId: req.decoded.id
        },
        limit: 1
    }).then(result => {
        res.json(result.shift())
    }).catch(function (err) {
        res.json(err)
    });
};

// -- DELETE --
lists.delete = function(req, res) {
    db.List.destroy({
        where: {
            id: req.params.id,
            ownerId: req.decoded.id
        }
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

// -- UPDATE --
lists.update = function(req, res) {
    db.List.update({
        name: req.body.name
    }, {
        where: {
            id: req.params.id,
            ownerId: req.decoded.id
        }
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

// -- POST --
lists.create = function(req, res) {
    db.List.create({
        name: req.body.name,
        ownerId: req.decoded.id
    }).then(result => {
        res.json(result)
    }).catch(function (err) {
        res.json(err)
    });
};

module.exports = lists;

'use strict';

const express = require('express'),
      router  = express.Router(),
      db      = require('../models'),
      jwt     = require('jsonwebtoken'),
      config  = require('../config/config');


//Sign in
router.post('/api/signin', function(req, res) {
  db.User.findOne({ where: {email: req.body.email} }).then(user => {
    if (user && user.password_hash && user.authenticate(req.body.password)) {
        jwt.sign({id: user.id, email: user.email}, config.secret, { expiresIn: '24h' }, function(err, token) {
            res.json({
                success: true,
                token: token
            });
        });
    } else {
        res.json({ success: false, message: 'Authentication failed.' })
    }
    
  }).catch(function (err) {
      res.json(err)
  });
});

//Sign up
router.post('/api/signup', function(req, res) {
    db.User.create(
        {
            name: req.body.name,
            email: req.body.email,
            password: req.body.password,
            password_confirmation: req.body.password_confirmation
        }
    ).then(user => {
        jwt.sign({id: user.id, email: user.email}, config.secret, { expiresIn: '24h' }, function(err, token) {
            res.json({
                success: true,
                token: token
            });
        });

    }).catch(function (err) {
        res.json(err)
    });
});


module.exports = router
'use strict';

const bcrypt = require('bcrypt');
const SALT_FACTOR = 5;

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [
        {name: 'demo', email: 'demo@demo.com', password_hash: this.genPassword('123'), createdAt: new Date(), updatedAt: new Date()}
    ], {});
  },

  down: function (queryInterface, Sequelize) {

  },

  genPassword: function(pass) {
    let salt = bcrypt.genSaltSync(SALT_FACTOR);
    let hash = bcrypt.hashSync(pass, salt);
    return hash;
  }
};

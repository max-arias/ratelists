"use strict";

const express = require('express'),
      app     = express(),
      router  = express.Router(),
      bodyParser = require('body-parser');

// Serve static files from client dist
app.use(express.static(__dirname + '/client/dist'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

//De-authed routes
app.use(require('./controllers/auth'))

//Enable auth on the following routes
app.use(require('./middleware'))

//Name-space all routes to api
app.use('/api', router);
app.use(require('./routes'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.json({
    message: err.message,
    error: (app.get('env') === 'development') ? err : {}
  });
  next(err)
});

module.exports = app;
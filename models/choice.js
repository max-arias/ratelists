"use strict";

module.exports = function(sequelize, DataTypes) {
    const Choice = sequelize.define('Choice', {
        value: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
                len: [1, 255]
            }
        },
        choiceType: {
            type: DataTypes.ENUM,
            values: ['negative', 'neutral', 'positive'],
            allowNull: false
        }
    });

    Choice.associate = function(models) {
        Choice.belongsTo(models.User, {foreignKey: { allowNull: false }});
        Choice.belongsTo(models.List, {foreignKey: { allowNull: false }});
    }

    return Choice;
}
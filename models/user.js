"use strict";

const bcrypt = require('bcrypt');

module.exports = function(sequelize, DataTypes) {
    const User = sequelize.define('User', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                notEmpty: true,
                len: [1, 50]
            }
        },
        email: {
            type: DataTypes.STRING,
            allowNull: false,
            validate: {
                isEmail: true,
                notEmpty: true,
                len: [1,255]
            }
        },
        password: {
            type: DataTypes.VIRTUAL, //virtual isn't saved
            allowNull: false,
            validate: {
                notEmpty: true
            }
        },
        password_hash: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true
            }
        },
        password_confirmation: {
            type: DataTypes.VIRTUAL,
            allowNull: false,
            validate: {
                notEmpty: true
            }
        }
    }, {
        indexes: [{
            unique: true,
            fields: ['email']
        }],
        hooks: {
            beforeCreate: (user)  => {
                user.email = user.email.toLowerCase();
                return hasSecurePassword(user);
            },
            beforeUpdate: (user)  => {
                user.email = user.email.toLowerCase();
                return hasSecurePassword(user);
            }
        }
    });

    User.associate = function(models) {
        User.belongsToMany(models.List, {through: 'UserList'});
    }

    User.prototype.authenticate = function(value) {
        if (bcrypt.compareSync(value, this.password_hash)) {
            return this;
        } else {
            return false;
        }
    };

    const hasSecurePassword = function(user) {
        const SALT_FACTOR = 5;
        
        if (user.password) {
            // If passwords don't match, error
            if (user.password !== user.password_confirmation) {
                return sequelize.Promise.reject("Password confirmation doesn't match Password");
            }

            // Get Salt, encrypt, set value (or return error)
            return bcrypt.genSalt(SALT_FACTOR).then(function(salt) {
                return bcrypt.hash(user.password, salt, null);
            }).then(function(hash) {
                user.setDataValue('password_hash', hash);
            }).catch(function(err) {
                return sequelize.Promise.reject(err);
            });
        }
    };

    return User;
}
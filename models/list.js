"use strict";

module.exports = function(sequelize, DataTypes) {
	const List = sequelize.define('List', {
		name: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				notEmpty: true,
				len: [1, 255]
			}
		}
	});

	List.associate = function(models) {
		List.belongsTo(models.User, {as: 'owner', foreignKey: { allowNull: false }});
        List.belongsToMany(models.User, {through: 'UserList', foreignKey: { allowNull: false }});
    }

	return List;
}
import router from '../router'

const LOGIN_URL = '/api/signin'
const SIGNUP_URL = '/api/signup'

export default {
  user: {
    authenticated: false
  },

  performAction (context, creds, redirect, action) {
    context.$http.post(action, creds).then(response => {
      localStorage.setItem('id_token', response.body.token)

      this.user.authenticated = true

      if (redirect) {
        router.go(redirect)
      }
    }, response => {
      context.error = response
    })
  },

  login (context, creds, redirect) {
    this.performAction(context, creds, redirect, LOGIN_URL)
  },

  signup (context, creds, redirect) {
    this.performAction(context, creds, redirect, SIGNUP_URL)
  },

  logout () {
    localStorage.removeItem('id_token')
    this.user.authenticated = false
    router.go('/')
  },

  checkInitialAuth () {
    var jwt = localStorage.getItem('id_token')
    if (jwt) {
      this.user.authenticated = true
    } else {
      this.user.authenticated = false
    }
  },

  getAuthHeader () {
    return {
      'Authorization': localStorage.getItem('id_token')
    }
  }
}

import Vue from 'vue'
import Vuex from 'vuex'

import actions from '../vuex/utils/api'

Vue.use(Vuex)

// http://jsbin.com/mihemezoya/edit?html,js,output

const LOGIN = 'LOGIN'
const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
const SIGNUP = 'SIGNUP'
const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS'
const LOGOUT = 'LOGOUT'

export const store = new Vuex.Store({
  state: {
    isLoggedIn: localStorage.getItem('id_token')
  },
  mutations: {
    [LOGIN] (state) {
      state.pending = true
    },
    [LOGIN_SUCCESS] (state) {
      state.isLoggedIn = true
      state.pending = false
    },
    [LOGOUT] (state) {
      state.isLoggedIn = false
    },
    [SIGNUP] (state) {
      state.pending = true
    },
    [SIGNUP_SUCCESS] (state) {
      state.isLoggedIn = true
      state.pending = false
    }
  },
  actions: {
    login ({state, commit, rootState}, creds) {
      console.log('login...', creds)
      commit(LOGIN) // show spinner

      return actions.post('/api/signin', creds).then(response => {
        localStorage.setItem('id_token', response.body.token)
        this.user.authenticated = true
        commit(LOGIN_SUCCESS)
      }, response => {
        this.error = response.body
      })
    },
    signup ({state, commit, rootState}, creds) {
      console.log('signup...', creds)
      commit(SIGNUP) // show spinner
      return actions.post('/api/signup', creds).then(response => {
        localStorage.setItem('id_token', response.body.token)
        this.user.authenticated = true
        commit(SIGNUP_SUCCESS)
      }, response => {
        this.error = response.body
      })
    },
    logout ({commit}) {
      localStorage.removeItem('id_token')
      commit(LOGOUT)
    }
  },
  getters: {
    isLoggedIn: state => {
      return state.isLoggedIn
    }
  }
})

import Vue from 'vue'

// Components
import Router from 'vue-router'
import Home from '@/components/Home'
import Create from '@/components/Create'
import User from '@/components/User'
import Login from '@/components/Login'
import SignUp from '@/components/SignUp'
import Landing from '@/components/Landing'

import auth from '@/services/auth'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      beforeEnter: (to, from, next) => {
        if (!auth.user.authenticated) {
          next('/')
        } else {
          next()
        }
      }
    },
    {
      path: '/create',
      name: 'Create',
      component: Create,
      beforeEnter: (to, from, next) => {
        if (!auth.user.authenticated) {
          next('/')
        } else {
          next()
        }
      }
    },
    {
      path: '/user',
      name: 'User',
      component: User,
      beforeEnter: (to, from, next) => {
        if (!auth.user.authenticated) {
          next('/')
        } else {
          next()
        }
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      beforeEnter: (to, from, next) => {
        if (auth.user.authenticated) {
          next('/home')
        } else {
          next()
        }
      }
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp,
      beforeEnter: (to, from, next) => {
        if (auth.user.authenticated) {
          next('/home')
        } else {
          next()
        }
      }
    }
  ]
})

# compromiseeverything

> Share and compare lists, find what you agree and disagree with

## To do

Home
  - List of lists?
Backend
  - Node?
  - Rails?
Profile
  - [ ] User Profile
  - [ ] User Lists
  - CRUD
    - [ ] Read
    - [ ] Create
    - [ ] Update
    - [ ] Delete
List
  - CRUD
    - [ ] Read
    - [ ] Create
    - [ ] Update
    - [ ] Delete

Things to look into:

Swype to agree/disagree: https://github.com/vuejs/vue-touch/tree/next

Template used: https://github.com/vuejs-templates/pwa

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```


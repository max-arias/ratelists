# compromiseeverything

> Share and compare lists, find what you agree and disagree with

## To do

Home
  - List of lists?
Backend
  - Node
    - Express
    - Sequelize
    - Passportjs
Profile
  - [ ] User Profile
  - [ ] User Lists
  - CRUD
    - [ ] Read
    - [ ] Create
    - [ ] Update
    - [ ] Delete
List
  - CRUD
    - [ ] Read
    - [ ] Create
    - [ ] Update
    - [ ] Delete

Things to look into:

 - Heroku prod deploy
 - Server /client/dist/index.html static files
 - Swype to agree/disagree: https://github.com/vuejs/vue-touch/tree/next

"use strict";

let express     = require('express')
  , choicesCtrl = require('../controllers/choices')
  , router      = express.Router();

// -- GET --
router.get('/', choicesCtrl.getAll);
router.get('/:listId', choicesCtrl.getByList);
router.get('/:listId/:id', choicesCtrl.get);

// -- DELETE --
router.delete('/:id', choicesCtrl.delete);

// -- UPDATE --
router.put('/:id', choicesCtrl.update);

// -- POST --
router.post('/:listId', choicesCtrl.create);

module.exports = router

const express = require('express'),
      router  = express.Router();

// Routes
router.use('/lists', require('./lists'))
router.use('/choices', require('./choices'))

module.exports = router
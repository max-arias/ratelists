"use strict";

let express   = require('express')
  , listsCtrl = require('../controllers/lists')
  , router    = express.Router();

// -- GET --
router.get('/', listsCtrl.getAll);
router.get('/:id', listsCtrl.get);

// -- DELETE --
router.delete('/:id', listsCtrl.delete);

// -- UPDATE --
router.put('/:id', listsCtrl.update);

// -- POST --
router.post('/:listId', listsCtrl.create);

module.exports = router